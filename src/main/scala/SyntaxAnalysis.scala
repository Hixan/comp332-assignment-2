/*
 * This file is part of COMP332 Assignment 2 2018.
 *
 * Lintilla, a simple functional programming language.
 *
 * © 2018, Dominic Verity and Anthony Sloane, Macquarie University.
 *         All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Parser for the Lintilla language.
 */

package lintilla

import org.bitbucket.inkytonik.kiama.parsing.Parsers
import org.bitbucket.inkytonik.kiama.util.Positions

/**
 * Module containing parsers for Lintilla.
 */
class SyntaxAnalysis (positions : Positions)
    extends Parsers (positions) {

  import LintillaTree._
  import scala.language.postfixOps


  // Top level parser.
  lazy val parser : PackratParser[Program] =
    phrase(program)

  // Parses a whole Lintilla program.
  lazy val program : PackratParser[Program] =
    repsep(exp,";") ^^ Program
//*
  lazy val exp : PackratParser[Expression] =
    assign |
    pexp1 |
    ifexp |
    block |
    whileexp |
    fndecl |
    returnexp |
    letdecl |
    letmutdecl |
    fndecl
    // */

  // precedence level 1
  // tested
  lazy val pexp1 : PackratParser[Expression] =
    (pexp1 <~ "=") ~ pexp2 ^^ EqualExp |
    (pexp1 <~ "<") ~ pexp2 ^^ LessExp |
    pexp2

  // precedence level 2
  // tested
  lazy val pexp2 : PackratParser[Expression] =
    (pexp2 <~ "+") ~ pexp3 ^^ PlusExp |
    (pexp2 <~ "-") ~ pexp3 ^^ MinusExp |
    pexp3

  // precedence level 3
  // tested
  lazy val pexp3 : PackratParser[Expression] =
      (pexp3 <~ "/") ~ eval ^^ SlashExp |
      (pexp3 <~ "*") ~ eval ^^ StarExp |
    eval


  // a value that is evaluated without the use of binary operators.
  // tested
  lazy val eval : PackratParser[Expression] =
    "-" ~> pexp1 ^^ NegExp |
    "false" ^^^ BoolExp(false) |
    "true" ^^^ BoolExp(true) |
    block |
    integer ^^ (i => IntExp(i.toInt)) |
    "(" ~> exp <~ ")" |
      app

  // tested
  lazy val app : PackratParser[Expression] =
    (app <~ "(" ) ~ repsep(exp, ",")  <~ ")" ^^ AppExp |
    idnuse ^^ IdnExp

  // tested
  lazy val assign : PackratParser[AssignExp] =
    ( idnuse <~ ":=" ) ~ exp ^^ AssignExp

  // tested
  lazy val ifexp : PackratParser[IfExp] =
    (("if" ~> exp ~ block <~ "else") ~ block) ^^ IfExp

  // tested
  lazy val whileexp : PackratParser[WhileExp] =
    "while" ~> exp ~ block ^^ WhileExp

  // tested
  lazy val returnexp : PackratParser[Return] =
    "return" ~> (exp?) ^^ Return

  // tested
  lazy val letdecl : PackratParser[LetDecl] =
    "let" ~> ( idndef <~ "=" ) ~ exp ^^ LetDecl

  // tested
  lazy val letmutdecl : PackratParser[LetMutDecl] =
    ( "let" ~ "mut" ) ~> ( idndef <~ "=" ) ~ exp ^^ LetMutDecl

  // tested
  lazy val fndecl : PackratParser[FnDecl] =
    ( "fn" ~> idndef)  ~ ("("  ~> repsep(paramdecl, ",") <~ ")" ) ~ (("->" ~> tipe)?) ~ block ^^ FnDecl

  // tested
  lazy val paramdecl : PackratParser[ParamDecl] =
    (idndef <~ ":" ) ~ tipe ^^ ParamDecl

  // tested
  lazy val tipe : PackratParser[Type] =
    "unit" ^^^ UnitType()|
    "bool" ^^^ BoolType() |
    "int" ^^^ IntType() |
    ((("fn" ~ "(") ~> repsep(tipe, ",") <~ ")" ) ~ ("->" ~> tipe)) ^^ FnType |
    (("fn" ~ "(") ~> repsep(tipe, ",") <~ ")" ) ^^ (t => FnType(t, UnknownType())) | // implied unknown type when type not specified
    "(" ~> tipe <~ ")"

  // tested
  lazy val block : PackratParser[Block] =
    ("{" ~> repsep(exp, ";") <~ "}")  ^^ Block

  // Parses a literal integer.
  lazy val integer : PackratParser[String] =
    regex("[0-9]+".r)

  // Parses a defining occurence of an identifier.
  lazy val idndef : PackratParser[IdnDef] =
    identifier ^^ IdnDef

  // Parses an applied opccurence of an identifier.
  lazy val idnuse : PackratParser[IdnUse] =
    identifier ^^ IdnUse

  // Parses a legal identifier. Checks to ensure that the word parsed is
  // not a Lintilla keyword.
  lazy val identifier : PackratParser[String] =
    (not(keyword) | failure("identifier expected but keyword found")) ~>
      "[a-zA-Z][a-zA-Z0-9_]*".r

  // Parses any legal Lintilla keyword. This parser ensures that the keyword found
  // is not a prefix of an longer identifier. So this parser will not parse the
  // "int" prefix of "integer" as a keyword.
  lazy val keyword =
    keywords("[^a-zA-Z0-9_]".r,
             List("bool", "else", "false", "fn", "if", "int", "let", "mut",
                  "return", "true", "unit", "while")) |
      failure("expecting keyword")

  // We use the character class `\R` here to match line endings, so that we correctly
  // handle all of the end-line variants in un*x, MacOSX, MS Windows, and unicode.
  override val whitespace: Parser[String] =
    """(\s|(//.*(\R|\z)))*""".r
}
