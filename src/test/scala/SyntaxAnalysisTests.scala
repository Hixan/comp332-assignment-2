/*
 * This file is part of COMP332 Assignment 2 2018.
 *
 * Lintilla, a simple functional programming language.
 *
 * © 2018, Dominic Verity and Anthony Sloane, Macquarie University.
 *         All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Tests of the parser of the Lintilla language.
 */

package lintilla

import org.bitbucket.inkytonik.kiama.util.ParseTests
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
  * Tests that check that the parser works correctly.  I.e., it accepts correct
  * input and produces the appropriate trees, and it rejects illegal input.
  */
@RunWith(classOf[JUnitRunner])
class SyntaxAnalysisTests extends ParseTests {

  import LintillaTree._

  val parsers = new SyntaxAnalysis(positions)
  import parsers._

  // Tests of parsing terminals

  test("parsing an identifier of one letter produces the correct tree") {
    identifier("x") should parseTo[String]("x")
  }

  test("parsing an identifier as an applied instance produces the correct tree") {
    idnuse("count") should parseTo[IdnUse](IdnUse("count"))
  }

  test("parsing an identifier containing digits and underscores produces the correct tree") {
    idndef("x1_2_3") should parseTo[IdnDef](IdnDef("x1_2_3"))
  }

  test("parsing an integer as an identifier gives an error") {
    identifier("42") should
    failParseAt(1, 1,
                "string matching regex '[a-zA-Z][a-zA-Z0-9_]*' expected but '4' found")
  }

  test("parsing a non-identifier as an identifier gives an error (digit)") {
    identifier("4foo") should
    failParseAt(1, 1,
                "string matching regex '[a-zA-Z][a-zA-Z0-9_]*' expected but '4' found")
  }

  test("parsing a non-identifier as an identifier gives an error (underscore)") {
    identifier("_f3") should
    failParseAt(1, 1,
                "string matching regex '[a-zA-Z][a-zA-Z0-9_]*' expected but '_' found")
  }

  test("parsing a keyword as an identifier gives an error") {
    identifier("let ") should failParseAt(1, 1,
                                          "identifier expected but keyword found")
  }

  test("parsing a keyword prefix as an identifier produces the correct tree") {
    identifier("letter") should parseTo[String]("letter")
  }

  test("parsing an integer of one digit as an integer produces the correct tree") {
    integer("8") should parseTo[String]("8")
  }

  test("parsing an integer as an integer produces the correct tree") {
    integer("99") should parseTo[String]("99")
  }

  test("parsing a non-integer as an integer gives an error") {
    integer("total") should
    failParseAt(1, 1,
                "string matching regex '[0-9]+' expected but 't' found")
  }

  // compare compiled trees to actual trees in resources file


  // file management code inspired by the blog posts:
  // https://alvinalexander.com/scala/how-to-list-files-in-directory-filter-names-scala
  // https://alvinalexander.com/scala/how-to-open-read-text-files-in-scala-cookbook-examples
  // https://www.journaldev.com/8278/scala-file-io-write-file-read-file

  // for whatever reason I could not get util.FileSource to import
  import java.io.{File, PrintWriter}
  import io.Source.fromFile
  import org.bitbucket.inkytonik.kiama.util.FileSource
  import org.bitbucket.inkytonik.kiama.parsing.{Success, NoSuccess}
  import org.bitbucket.inkytonik.kiama.output.PrettyPrinter.{layout, any}
  import org.bitbucket.inkytonik.kiama.util.Messaging.{message => message1}


  val test_path = "./src/test/resources"
  val test_res = new File(test_path).listFiles.toList
  // println(test_res(0).getClass)

  //* remove first slash to comment below tests out.
  // filter files to make sure each file is a lin file, and it has a tree file with the same name.
  test_res.filter((f: File) => f.getName.matches(".*\\.lin") && new File(f.toString.dropRight(3)+"tree").exists).foreach(
    (f: File) => {
      // remove carrage returns,
      val correct_tree = fromFile(f.toString.dropRight(3) + "Tree").mkString.replaceAll("\r", "").stripLineEnd
      val src = FileSource(f.toString)
      test(f.getName + " test") {

        val parsers = new SyntaxAnalysis (positions)

        // Parse the file and format it in the same way as tests expect
        // taken from Main method
        val result_tree = parsers.parse(parsers.parser, src) match {
          case Success(result, _) => ("Program tree:\n-------------\n\n" + layout(any(result))).stripLineEnd
          case f : NoSuccess =>
              val pos = f.next.position
              positions.setStart(f, pos)
              positions.setFinish(f, pos)
              val messages = message1(f, f.message)
              formatMessages(messages)
        }

        // write to file so comparison programs can be used.
        //*
        val writer = new PrintWriter(new File(f.toString.dropRight(3) + "result"))
        writer.write(result_tree)
        writer.close()
        // */

        result_tree shouldEqual correct_tree
      }
    })
    // */
  println("The following lin files have no associated tree file:")
  test_res.filter((f: File) => f.getName.matches(".*\\.lin") && !new File(f.toString.dropRight(3)+"tree").exists) foreach println

  // single matcher testing

  // test integer literals (eval, integer)
  test("name") {
    exp("0") should parseTo[Expression](IntExp(0))
  }
  test("parse int 1") {
    exp("1") should parseTo[Expression](IntExp(1))
  }
  test("parse int 2") {
    exp("47") should parseTo[Expression](IntExp(47))
  }
  test("parse int fail 3") {
    exp("_3") should failParseAt(1, 1, "'fn' expected but '_' found")
  }
  /*
  test("name") {
    exp("99999999999999999999999999999999999999999999999999999") should failParseAt(1, 1, "")
  }
  // this test is not something I can fix (Its probelm is not in the SyntaxAnalysis.scala file.)
  // */

  // test boolean literals (eval)
  test("literals true") {
    exp("true") should parseTo[Expression](BoolExp(true))
  }
  test("literals false") {
    exp("false") should parseTo[Expression](BoolExp(false))
  }

  // test type literals (tipe)
  test("literal type unit") {
    tipe("unit") should parseTo[Type](UnitType())
  }
  test("literal type bool") {
    tipe("bool") should parseTo[Type](BoolType())
  }
  test("literal type int") {
    tipe("int") should parseTo[Type](IntType())
  }
  test("literal type bracketed") {
    tipe("(int)") should parseTo[Type](IntType())
  }
  test("literal type function") {
    tipe("fn () -> unit") should parseTo[Type](FnType(Vector(), UnitType()))
  }
  test("literal type function with arguments") {
    tipe("fn (bool) -> int") should parseTo[Type](FnType(Vector(BoolType()), IntType()))
  }
  test("literal type function with arguments that return function") {
    tipe("fn (int, bool, bool, unit) -> fn(int, bool, bool, int) -> unit") should parseTo[Type](
      FnType(Vector(IntType(), BoolType(), BoolType(), UnitType()),
        FnType(Vector(IntType(), BoolType(), BoolType(), IntType()), UnitType())
      )
    )
  }

  // test negative expressions (eval)
  test("negative expression 3") {
    exp("-3") should parseTo[Expression](NegExp(IntExp(3)))
  }
  test("negative expression x") {
    exp("-x") should parseTo[Expression](NegExp(IdnExp(IdnUse("x"))))
  }
  test("negative expression 9999") {
    exp("-9999") should parseTo[Expression](NegExp(IntExp(9999)))
  }
  test("negative expression false") {
    exp("-false") should parseTo[Expression](NegExp(BoolExp(false)))
  }

  // test variable declarations (letdecl)
  test("letdecl fail 1") {
    letdecl("let 3 = 4") should failParseAt(1, 5, "string matching regex '[a-zA-Z][a-zA-Z0-9_]*' expected but '3' found")
  }
  test("letdecl pass literal") {
    letdecl("let y = 388") should parseTo[LetDecl](LetDecl(IdnDef("y"), IntExp(388)))
  }
  test("letdecl complex name literal") {
    letdecl("let tets = 400") should parseTo[LetDecl](LetDecl(IdnDef("tets"), IntExp(400)))
  }
  test("letdecl invalid name fail") {
    letdecl("let _test = 40") should failParseAt(1, 5, "string matching regex '[a-zA-Z][a-zA-Z0-9_]*' expected but '_' found")
  }
  test("letdecl invalid name fail 2") {
    letdecl("let 3abc = 0") should failParseAt(1, 5, "string matching regex '[a-zA-Z][a-zA-Z0-9_]*' expected but '3' found")
  }
  test("letdecl no assignment") {
    letdecl("let x") should failParseAt(1, 6, "'=' expected but end of source found")
  }

  // test variable access (app, idnuse)
  test("variable access 1") {
    exp("x + 4") should parseTo[Expression](PlusExp(IdnExp(IdnUse("x")), IntExp(4)))
  }
  test("variable access 2") {
    exp("y + 2") should parseTo[Expression](PlusExp(IdnExp(IdnUse("y")), IntExp(2)))
  }
  test("variable access invalid name") {
    exp("_a + b") should failParseAt(1, 1, "'fn' expected but '_' found")
  }

  // test bracketing (eval)
  test("brackets unchanged order") {
    exp("(4+5)+6") should parseTo[Expression](PlusExp(PlusExp(IntExp(4), IntExp(5)), IntExp(6)))
  }
  test("brackets changed order") {
    exp("4+(5+6)") should parseTo[Expression](PlusExp(IntExp(4), PlusExp(IntExp(5), IntExp(6))))
  }
  test("brackets changed precedence") {
    exp("(4+5)*6") should parseTo[Expression](StarExp(PlusExp(IntExp(4), IntExp(5)), IntExp(6)))
  }
  test("brackets changed precedence and order") {
    exp("4/(3+8)") should parseTo[Expression](SlashExp(IntExp(4), PlusExp(IntExp(3), IntExp(8))))
  }
  test("brackets negexp") {
    exp("-(24+y)") should parseTo[Expression](NegExp(PlusExp(IntExp(24), IdnExp(IdnUse("y")))))
  }

  // test mutable variable declarations (letmutdecl)
  test("mut literal") {
    exp("let mut x = 3") should parseTo[Expression](LetMutDecl(IdnDef("x"), IntExp(3)))
  }
  test("mut no assignment") {
    exp("let mut x") should failParseAt(1, 10, "'=' expected but end of source found")
  }

  // test parameter declarations (paramdecl)
  test("parameter valid") {
    paramdecl("a: bool") should parseTo[ParamDecl](ParamDecl(IdnDef("a"), BoolType()))
  }
  test("parameter invalid name") {
    paramdecl("_abc: int") should failParseAt(1, 1, "string matching regex '[a-zA-Z][a-zA-Z0-9_]*' expected but '_' found")
  }
  test("parameter invalid type") {
    paramdecl("abc: test") should failParseAt(1, 6, "'(' expected but 't' found")
  }

  // test function declarations (fndecl)
  test("basic function") {
    exp("fn test() -> unit {}") should parseTo[Expression](FnDecl(IdnDef("test"), Vector(), Option(UnitType()), Block(Vector())))
  }
  test("function invalid name") {
    exp("fn 123(c : bool) -> unit") should failParseAt(1, 4, "string matching regex '[a-zA-Z][a-zA-Z0-9_]*' expected but '1' found")
  }
  test("function name in brackets") {
    exp("fn (x)(b : int) -> unit") should failParseAt(1, 4, "string matching regex '[a-zA-Z][a-zA-Z0-9_]*' expected but '(' found")
  }
  test("function multiple parameters") {
    exp("fn x(a : unit, b:int, c:bool) -> int {}") should parseTo[Expression](FnDecl(IdnDef("x"), Vector(),Option(IntType()),Block(Vector())))
  }
  test("function no type returned") {
    exp("fn x() {}") should parseTo[Expression](FnDecl(IdnDef("x"), Vector(),Option(), Block(Vector())))
  }

  // test single function calls (app)
  "x()"
  "abc(test)"
  "abc(true)"
  "abc(true, false, 35, xyz)"

  // test multi function calls (app)
  "x()()"
  "abc(ax, 3)(test)"
  "abc(aoeuidhtns)(true)"
  "abc(true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false)(true, false, 35, xyz)"

  // test variable assignment (assign)
  "x := 30"
  "y := x()"

  // test if expression (ifexp)
  "if 3 {} else {}"
  "if a + b {} else {}"
  "if true < 55 {abc; def} else {qwerty}"

  // test while expression (whileexp)
  "while 3 {} else {}"
  "while a + b {} else {}"
  "while true < 55 {abc; def} else {qwerty}"

  // test return expression (returnexp)
  "return x"
  "return x()"
  "return true"
  "return"

  // test block definitions
  "{}"
  "{abc; de}"
  "{abc; def; {abc; def}; dx + dy}"

  // test associativity (pexp[1, 2, 3])
  test("associativity (/)") {
    exp("0/5/7") should parseTo[Expression](SlashExp(SlashExp(IntExp(0),IntExp(5)),IntExp(7)))
  }
  test("associativity (*)") {
    exp("0*5*7") should parseTo[Expression](StarExp(StarExp(IntExp(0),IntExp(5)),IntExp(7)))
  }
  test("associativity (+)") {
    exp("0+5+7") should parseTo[Expression](PlusExp(PlusExp(IntExp(0),IntExp(5)),IntExp(7)))
  }
  test("associativity (-)") {
    exp("0-5-7") should parseTo[Expression](MinusExp(MinusExp(IntExp(0),IntExp(5)),IntExp(7)))
  }
  test("associativity (*/)") {
    exp("0*5/7") should parseTo[Expression](SlashExp(StarExp(IntExp(0),IntExp(5)),IntExp(7)))
  }
  test("associativity (/*)") {
    exp("0/5*7") should parseTo[Expression](StarExp(SlashExp(IntExp(0),IntExp(5)),IntExp(7)))
  }
  test("associativity (+-)") {
    exp("0+5-7") should parseTo[Expression](MinusExp(PlusExp(IntExp(0),IntExp(5)),IntExp(7)))
  }
  test("associativity (-+)") {
    exp("0-5+7") should parseTo[Expression](PlusExp(MinusExp(IntExp(0),IntExp(5)),IntExp(7)))
  }

}

